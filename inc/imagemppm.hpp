#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP

#include "imagem.hpp"
#include <string>
#include <fstream>

using namespace std;

class ImagemPpm : public Imagem{
  //Atributos
  private:
    ifstream arquivoppm;
    ofstream saidappm;
    string linhappm, coment;
    

  public:
    ImagemPpm();
    ImagemPpm(string url); //sobrecarga do construtor
    ~ImagemPpm();

    //Métodos funcionais
    void fechaArquivo();
    void lerDados(string url);


};

#endif
