#ifndef IMAGEMPGM_HPP
#define IMAGEMPGM_HPP

#include "imagem.hpp"
#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>

using namespace std;

class ImagemPgm : public Imagem{
    //Atributos
    private:
    ifstream arquivopgm;
    ofstream saida;
    string linha, comentario;

    
    public:
    ImagemPgm();
    ImagemPgm(string url); //sobrecarga do construtor
    ~ImagemPgm();

    //Métodos funcionais
    void fechaArquivo();
    void lerDados(string url);

};
#endif