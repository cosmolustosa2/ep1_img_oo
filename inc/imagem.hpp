#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
using namespace std;

class Imagem{
  //Atributos
  private:
    string tipo;
    int largura;
    int altura;
    int valorMaximo;

  //Métodos
  public:
    Imagem(); //Construtor
    ~Imagem(); //Destrutor

    //Métodos Acessores
    void setTipo(string tipo);
    string getTipo();
    void setLargura(int largura);
    int getLargura();
    void setAltura(int altura);
    int getAltura();
    void setValorMaximo(int valorMaximo);
    int getValorMaximo();

    //Métodos Funcionais
    virtual void fechaArquivo() = 0;
    virtual void lerDados(string url) = 0;


};
#endif
