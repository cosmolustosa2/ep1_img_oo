Para compilar o programa é necessário apenas um comando make 
na pasta raiz da aplicação pelo terminal do UNIX


Ao iniciar a aplicação o usuário indicará qual formato 
de imagem deseja trabalhar com uma das opções abaixo
1 - Abrir PPM
2 - Abrir PGM
3 - Sair

Após definir o tipo de imagem o usuario deverá digitar
o caminho do arquivo conforme exemplo a seguir

home/user/dir/arquivo.extensao

para sair da aplicacao o usuario deverá escolher a
opção 3.

