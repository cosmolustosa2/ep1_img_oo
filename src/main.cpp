#include <iostream>
#include <string>
#include "imagemppm.hpp"
#include "imagempgm.hpp"
#include "imagem.hpp"

using namespace std;

/*Protótipo da função*/
void abreMenu();

int main(int argc, char ** argv){
    
    //chama o menu
    abreMenu();

    return 0;
}

void abreMenu(){
    int selecao;
    
    do{
        cout << "Escolha uma opção abaixo!" << endl;
        cout << "1 - Abrir PPM" << endl;
        cout << "2 - Abrir PGM" << endl;
        cout << "3 - Sair" << endl;
       
        cin >> selecao;
        
        switch(selecao){
        case 1: {
                string path;
                cout << "Digite o caminho do arquivo seguido da extensao do arquivo!" << endl;
                cin >> path;
                ImagemPpm imgppm(path);
                imgppm.lerDados(path);
             }
        break;

        case 2: {
                string path2;          
                cout << "Digite o caminho do arquivo seguido da extensao do arquivo!" << endl;
                cin >> path2;
                ImagemPgm imgpgm(path2);
                imgpgm.lerDados(path2);
            }
        break;
        
        }
    }while(selecao != 3);

}
