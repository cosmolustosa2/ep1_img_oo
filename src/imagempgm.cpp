#include <iostream>
#include "imagem.hpp"
#include "imagempgm.hpp"
#include <cstdio>
#include <cstdlib>

using namespace std;

ImagemPgm::ImagemPgm(string url){    
    setTipo("");
    setAltura(2);
    setLargura(2);
    setValorMaximo(2);

}
ImagemPgm::~ImagemPgm(){

}
void ImagemPgm::fechaArquivo(){
    arquivopgm.close();
 }
void ImagemPgm::lerDados(string url){
    ifstream arquivopgm(url, ios::in);
    ofstream saida("bin/dados.txt", ios::out);
    if(arquivopgm.is_open()){

        while(getline(arquivopgm, linha)){
            if((linha[0] == 'P')){
                setTipo(linha);
                continue;

            }else if((linha[0]== '#')){
                comentario = linha;
                continue;
            }else{
                saida << linha << endl;
                cout << linha << endl;
            }

        }
    }else{
        cout << "Não foi possivel abrir o arquivo!" << endl;
    }
    saida.close();
 }