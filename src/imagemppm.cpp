#include <iostream>
#include <fstream>
#include <string>

#include "imagemppm.hpp"
#include "imagem.hpp"

using namespace std;

ImagemPpm::ImagemPpm(string url){
  setTipo("");
  setAltura(2);
  setLargura(2);
  setValorMaximo(2);

}
ImagemPpm::~ImagemPpm(){

}
void ImagemPpm::fechaArquivo(){
   arquivoppm.close();
 }
 void ImagemPpm::lerDados(string url){

    ifstream arquivoppm(url, ios::in);
    ofstream saidappm("bin/dadosppm.txt", ios::out);
    if(arquivoppm.is_open()){

        while(getline(arquivoppm, linhappm)){
            if((linhappm[0] == 'P')){
                setTipo(linhappm);
                continue;

            }else if((linhappm[0]== '#')){
                coment = linhappm;
                continue;
            }else{
                saidappm << linhappm << endl;
                cout << linhappm << endl;
            }

        }
    }else{
        cout << "Não foi possivel abrir o arquivo!" << endl;
    }

    saidappm.close();

 }
